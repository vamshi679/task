import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';


const material =[
  MatButtonModule,MatToolbarModule,MatCardModule,MatFormFieldModule,MatInputModule,MatIconModule,MatSnackBarModule,MatTableModule,MatPaginatorModule
]


@NgModule({
  imports: [material,ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})],
  exports:[material]
})
export class MaterialModule { }

imports: [
  
]