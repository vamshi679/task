import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  constructor(private ds:DataService,private router:Router) { }

  ngOnInit(){    
  }

  checkUserStatus(){
    
    if(this.ds.userLoginStatus==false){
      alert('Oops...! you are not loggedin please login to see private information')
      this.router.navigate(['/login'])
    }
    else{
      this.router.navigate(['/resource'])
    }
  }
  
}

