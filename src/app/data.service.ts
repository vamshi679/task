import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  userLoginStatus: boolean = false;

  constructor(private hc: HttpClient) { }

  send(){
    return this.userLoginStatus
  }

  userlogin(x):Observable<any>{
    return this.hc.post('https://reqres.in/api/login',x)
  }

  logout(){
    localStorage.removeItem('token')
    this.userLoginStatus = false;
  }

  loggedIn(){
    return !!localStorage.getItem('token')
  }

}
